###Shopping Cart Application###

This is a simple front end shopping cart application made with angular 2
#Author: Georgios Kyriakou

###Description###
The application is made using the angular quickstart (https://github.com/angular/quickstart), so in order to strat it, simply type "npm start"

##Components##
landingPage-- Users are redirected here uppon visiting the page. They can choose from two mock items and add them into their shopping cart

shoppingCart-- Users can view and remove their current shopping cart, as well as submit it to the server

purchased-- After submitting the cart, users are redirected here to view details about their purchase

pageNotFound-- displays "page not found" message

##Services##
addToCart-- this handles the shopping cart completely, meaning that it adds, removes, provides info about the current cart to components and submits it to the server.

getToken-- this service is called by the app.component in the begining to fetch a web token from the server, which is then stored in the local storage

###Testing###
I have not had a lot of experience with testing and jasmine, but i have tried implementing tests for this application. Testing can be done with "npm test"