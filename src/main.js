"use strict";
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var app_module_1 = require("./app/app.module");
var addToCart_service_1 = require("./app/services/addToCart/addToCart.service");
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule, [addToCart_service_1.CartService]);
//# sourceMappingURL=main.js.map