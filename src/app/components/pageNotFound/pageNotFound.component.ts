import { Component } from '@angular/core';


@Component({
  moduleId: module.id,
  selector: 'pageNotFound',
  templateUrl: 'pageNotFound.component.html',
  styleUrls: ['pageNotFound.component.css']
})
export class PageNotFoundComponent { }
 
