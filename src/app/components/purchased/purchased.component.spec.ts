import {PurchaseComponent} from './purchased.component'
import {TestBed, inject} from '@angular/core/testing';



describe('>>Component: PurchaseComponent', () => {
  describe('Define Component', () => {
    it('defined',()=>{
      TestBed.configureTestingModule({
        declarations: [PurchaseComponent]
      })
      inject([PurchaseComponent],(prcComp:PurchaseComponent) => {
      expect(prcComp).toBeDefined()
      })
    })
  })

  describe('Get response', () => {
    it('got it',()=>{
      TestBed.configureTestingModule({
        declarations: [PurchaseComponent,PurchaseComponent]
      })
      inject([PurchaseComponent],(prcComp:PurchaseComponent) => {
        expect(prcComp.response).toEqual(0)
        prcComp.getRes()
        expect(prcComp.response).not.toBe(0)
      })
    })
  })
})
