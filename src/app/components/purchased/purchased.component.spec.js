"use strict";
var purchased_component_1 = require("./purchased.component");
var testing_1 = require("@angular/core/testing");
describe('>>Component: PurchaseComponent', function () {
    describe('Define Component', function () {
        it('defined', function () {
            testing_1.TestBed.configureTestingModule({
                declarations: [purchased_component_1.PurchaseComponent]
            });
            testing_1.inject([purchased_component_1.PurchaseComponent], function (prcComp) {
                expect(prcComp).toBeDefined();
            });
        });
    });
    describe('Get response', function () {
        it('got it', function () {
            testing_1.TestBed.configureTestingModule({
                declarations: [purchased_component_1.PurchaseComponent, purchased_component_1.PurchaseComponent]
            });
            testing_1.inject([purchased_component_1.PurchaseComponent], function (prcComp) {
                expect(prcComp.response).toEqual(0);
                prcComp.getRes();
                expect(prcComp.response).not.toBe(0);
            });
        });
    });
});
//# sourceMappingURL=purchased.component.spec.js.map