import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CartService} from '../../services/addToCart/addToCart.service'

@Component({
  moduleId: module.id,
  selector: 'purch',
  templateUrl: 'purchased.component.html',
  styleUrls: ['purchased.component.css'],

})
export class PurchaseComponent implements OnInit{
  response:any=0

  constructor(private router:Router,
              private crtService:CartService
              ){ }

  ngOnInit(){
     this.getRes()
    }
    getRes(){
      this.response= this.crtService.getResponse()
    }
  }
