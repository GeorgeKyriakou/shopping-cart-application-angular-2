"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var addToCart_service_1 = require("../../services/addToCart/addToCart.service");
var PurchaseComponent = (function () {
    function PurchaseComponent(router, crtService) {
        this.router = router;
        this.crtService = crtService;
        this.response = 0;
    }
    PurchaseComponent.prototype.ngOnInit = function () {
        this.getRes();
    };
    PurchaseComponent.prototype.getRes = function () {
        this.response = this.crtService.getResponse();
    };
    return PurchaseComponent;
}());
PurchaseComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'purch',
        templateUrl: 'purchased.component.html',
        styleUrls: ['purchased.component.css'],
    }),
    __metadata("design:paramtypes", [router_1.Router,
        addToCart_service_1.CartService])
], PurchaseComponent);
exports.PurchaseComponent = PurchaseComponent;
//# sourceMappingURL=purchased.component.js.map