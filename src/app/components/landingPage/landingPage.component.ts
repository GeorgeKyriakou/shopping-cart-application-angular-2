import { Component, OnInit } from '@angular/core';
import { CartService } from '../../services/addToCart/addToCart.service'

@Component({
  moduleId: module.id,
  selector: 'home',
  templateUrl: 'landingPage.component.html',
  styleUrls: ['landingPage.component.css']
})
export class LandingPageComponent implements OnInit {
  items:Object[]=[]
  cartSize:any = 0

  constructor(private _adService:CartService){ }

  //fragment the object array into chunks of 2 to display them in a 2-item-per-row
  //grid
  ngOnInit(){
    this._adService.getItemList().then(res=>{
       this.fragmentArr(res, 2)
     })
     this._adService.getCart().then(res=>{
       this.cartSize =res.length
     })
    }


  fragmentArr(arr:any,chunk:number){
    var ele = document.getElementById('itemGrid')
    let temporray
    let n =0
    for (let i=0,j=arr.length; i<j; i+=chunk) {
      temporray = arr.slice(i,i+chunk);
      this.items[n]= temporray
      n++
    }
  }

  addtoCart(id:number,quantity:number){
    this._adService.addToCart(id,quantity)
    this._adService.getCart().then(res=>{
      this.cartSize =res.length
    })
  }
}
