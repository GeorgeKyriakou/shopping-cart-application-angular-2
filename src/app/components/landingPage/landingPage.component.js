"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var addToCart_service_1 = require("../../services/addToCart/addToCart.service");
var LandingPageComponent = (function () {
    function LandingPageComponent(_adService) {
        this._adService = _adService;
        this.items = [];
        this.cartSize = 0;
    }
    //fragment the object array into chunks of 2 to display them in a 2-item-per-row
    //grid
    LandingPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._adService.getItemList().then(function (res) {
            _this.fragmentArr(res, 2);
        });
        this._adService.getCart().then(function (res) {
            _this.cartSize = res.length;
        });
    };
    LandingPageComponent.prototype.fragmentArr = function (arr, chunk) {
        var ele = document.getElementById('itemGrid');
        var temporray;
        var n = 0;
        for (var i = 0, j = arr.length; i < j; i += chunk) {
            temporray = arr.slice(i, i + chunk);
            this.items[n] = temporray;
            n++;
        }
    };
    LandingPageComponent.prototype.addtoCart = function (id, quantity) {
        var _this = this;
        this._adService.addToCart(id, quantity);
        this._adService.getCart().then(function (res) {
            _this.cartSize = res.length;
        });
    };
    return LandingPageComponent;
}());
LandingPageComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'home',
        templateUrl: 'landingPage.component.html',
        styleUrls: ['landingPage.component.css']
    }),
    __metadata("design:paramtypes", [addToCart_service_1.CartService])
], LandingPageComponent);
exports.LandingPageComponent = LandingPageComponent;
//# sourceMappingURL=landingPage.component.js.map