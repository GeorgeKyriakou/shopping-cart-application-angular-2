import {LandingPageComponent} from './landingPage.component'
import {TestBed,inject} from '@angular/core/testing';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';

describe('>>Component: LandingPageComponent', ()=>{
  describe('define component', ()=>{
  it('defined',()=>{

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LandingPageComponent],
            imports: [ReactiveFormsModule]
        })
      })
        inject([LandingPageComponent],(lndComp:any) => {
        expect(lndComp).toBeDefined()
      })
    })
  })
  describe('properties - unctions', ()=>{
    it('Item list length',()=>{
      beforeEach(() => {
          TestBed.configureTestingModule({
              declarations: [LandingPageComponent],
              imports: [ReactiveFormsModule]
          })
          inject([LandingPageComponent],(lndComp:any) => {
            expect(lndComp.items.length).toEqual(2);
            expect(lndComp.cartSize).toEqual(0);
            lndComp.addtoCart(88,1)
            expect(lndComp.cartSize).toEqual(1);
            lndComp.fragmentArr([1,2,4,3],2)
            expect(lndComp.items).toBe([[1,2],[4,3]])
        })

        })
    })
  })
})
