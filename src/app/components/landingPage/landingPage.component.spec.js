"use strict";
var landingPage_component_1 = require("./landingPage.component");
var testing_1 = require("@angular/core/testing");
var forms_1 = require("@angular/forms");
describe('>>Component: LandingPageComponent', function () {
    describe('define component', function () {
        it('defined', function () {
            beforeEach(function () {
                testing_1.TestBed.configureTestingModule({
                    declarations: [landingPage_component_1.LandingPageComponent],
                    imports: [forms_1.ReactiveFormsModule]
                });
            });
            testing_1.inject([landingPage_component_1.LandingPageComponent], function (lndComp) {
                expect(lndComp).toBeDefined();
            });
        });
    });
    describe('properties - unctions', function () {
        it('Item list length', function () {
            beforeEach(function () {
                testing_1.TestBed.configureTestingModule({
                    declarations: [landingPage_component_1.LandingPageComponent],
                    imports: [forms_1.ReactiveFormsModule]
                });
                testing_1.inject([landingPage_component_1.LandingPageComponent], function (lndComp) {
                    expect(lndComp.items.length).toEqual(2);
                    expect(lndComp.cartSize).toEqual(0);
                    lndComp.addtoCart(88, 1);
                    expect(lndComp.cartSize).toEqual(1);
                    lndComp.fragmentArr([1, 2, 4, 3], 2);
                    expect(lndComp.items).toBe([[1, 2], [4, 3]]);
                });
            });
        });
    });
});
//# sourceMappingURL=landingPage.component.spec.js.map