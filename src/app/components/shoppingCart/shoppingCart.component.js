"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var addToCart_service_1 = require("../../services/addToCart/addToCart.service");
var CartComponent = (function () {
    function CartComponent(_adService, router) {
        this._adService = _adService;
        this.router = router;
        this.cart = [];
        this.itemList = [];
        this.inCart = [];
    }
    CartComponent.prototype.ngOnInit = function () {
        this.getThisCart();
    };
    //get the array with article_id and article_quantity
    CartComponent.prototype.getThisCart = function () {
        var _this = this;
        this._adService.getCart().then(function (res) {
            _this.cart = res;
            _this.getItemList();
        });
    };
    //get the item list with complete details for all items
    CartComponent.prototype.getItemList = function () {
        var _this = this;
        this._adService.getItemList().then(function (res) {
            _this.itemList = res;
            _this.isInCart();
        });
    };
    //find the details for the items in our cart, to display on the page
    CartComponent.prototype.isInCart = function () {
        var i = 0;
        var _loop_1 = function (item) {
            var result = this_1.itemList.filter(function (obj) {
                return obj['article_id'] == item['article_id'];
            });
            //item['article_quantity']
            this_1.inCart[i] = [{ "itemName": result[0]["itemName"],
                    "providerName": result[0]["providerName"],
                    "price": result[0]["price"],
                    "article_quantity": item["article_quantity"],
                    "article_id": item["article_id"]
                }];
            i++;
        };
        var this_1 = this;
        for (var _i = 0, _a = this.cart; _i < _a.length; _i++) {
            var item = _a[_i];
            _loop_1(item);
        }
    };
    CartComponent.prototype.removeFromCart = function (id) {
        this._adService.removeFromCart(id);
        this.inCart.splice(this.inCart.indexOf(id), 1);
    };
    CartComponent.prototype.submitCart = function () {
        var _this = this;
        this._adService.submitCart().subscribe(function (res) {
            console.log(_this._adService.setResponse(res));
            _this.router.navigate(['my_cart/purchased']);
        });
    };
    return CartComponent;
}());
CartComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'cart',
        templateUrl: 'shoppingCart.component.html',
        styleUrls: ['shoppingCart.component.css'],
    }),
    __metadata("design:paramtypes", [addToCart_service_1.CartService,
        router_1.Router])
], CartComponent);
exports.CartComponent = CartComponent;
//# sourceMappingURL=shoppingCart.component.js.map