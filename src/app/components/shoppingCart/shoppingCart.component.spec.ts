import {CartComponent} from './shoppingCart.component'
import {LandingPageComponent} from '../landingPage/landingPage.component'
import { Router } from '@angular/router';
import {TestBed, inject, async} from '@angular/core/testing';

class MockRouter {
    navigateByUrl(url: '/my_cart') { return url; }
}

describe('>>Component: CartComponent', () => {
  describe('Define Component', () => {
    it('defined',()=>{
      TestBed.configureTestingModule({
        declarations: [CartComponent]
      })
      inject([CartComponent],(crtComp:CartComponent) => {
      expect(crtComp).toBeDefined()
      })
    })
  })

  describe('add to cart', () => {
    it('added',()=>{
      TestBed.configureTestingModule({
        declarations: [CartComponent,LandingPageComponent]
      })
      inject([CartComponent],(crtComp:CartComponent) => {
        let lndPage:LandingPageComponent
        expect(crtComp.cart.length).toEqual(0)
        lndPage.addtoCart(88,1)
        expect(crtComp.cart.length).toEqual(1)
      })
    })
  })

})
