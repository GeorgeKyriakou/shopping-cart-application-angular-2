import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../../services/addToCart/addToCart.service'
import { Articles } from '../../models/MyCart.model'

@Component({
  moduleId: module.id,
  selector: 'cart',
  templateUrl: 'shoppingCart.component.html',
  styleUrls: ['shoppingCart.component.css'],

})

export class CartComponent implements OnInit {
  cart:Articles[]=[]
  itemList:Object[]=[]
  inCart:Object[]=[]
  constructor(
    private _adService:CartService,
    private router:Router){}


    ngOnInit(){
      this.getThisCart()
    }
    //get the array with article_id and article_quantity
    getThisCart(){
      this._adService.getCart().then(res => {
        this.cart = res
        this.getItemList()
        })
    }
    //get the item list with complete details for all items
    getItemList(){
      this._adService.getItemList().then(res=>{
            this.itemList =res
            this.isInCart()
      })
    }

    //find the details for the items in our cart, to display on the page
    isInCart(){
      let i=0
      for(let item of this.cart){
        let  result = this.itemList.filter(function( obj ) {
            return obj['article_id'] == item['article_id'];
            })
            //item['article_quantity']
        this.inCart[i]=[{"itemName":result[0]["itemName"],
                          "providerName":result[0]["providerName"],
                          "price":result[0]["price"],
                          "article_quantity":item["article_quantity"],
                          "article_id":item["article_id"]
                          }]
         i++
      }
    }


     removeFromCart(id:string){
         this._adService.removeFromCart(id)
         this.inCart.splice(this.inCart.indexOf(id),1)

     }

     submitCart(){
        this._adService.submitCart().subscribe((res:Object)=>{
          console.log(this._adService.setResponse(res))
          this.router.navigate(['my_cart/purchased'])
        })
        }
  }
