"use strict";
var shoppingCart_component_1 = require("./shoppingCart.component");
var landingPage_component_1 = require("../landingPage/landingPage.component");
var testing_1 = require("@angular/core/testing");
var MockRouter = (function () {
    function MockRouter() {
    }
    MockRouter.prototype.navigateByUrl = function (url) { return url; };
    return MockRouter;
}());
describe('>>Component: CartComponent', function () {
    describe('Define Component', function () {
        it('defined', function () {
            testing_1.TestBed.configureTestingModule({
                declarations: [shoppingCart_component_1.CartComponent]
            });
            testing_1.inject([shoppingCart_component_1.CartComponent], function (crtComp) {
                expect(crtComp).toBeDefined();
            });
        });
    });
    describe('add to cart', function () {
        it('added', function () {
            testing_1.TestBed.configureTestingModule({
                declarations: [shoppingCart_component_1.CartComponent, landingPage_component_1.LandingPageComponent]
            });
            testing_1.inject([shoppingCart_component_1.CartComponent], function (crtComp) {
                var lndPage;
                expect(crtComp.cart.length).toEqual(0);
                lndPage.addtoCart(88, 1);
                expect(crtComp.cart.length).toEqual(1);
            });
        });
    });
});
//# sourceMappingURL=shoppingCart.component.spec.js.map