import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

import {Articles} from '../../models/MyCart.model'

import 'rxjs/add/operator/map';

@Injectable()
export class CartService {
  private itemList:Object[]=[{
    itemName:"Powerball Display" ,
    category:"electronics",
    price:"29.99 GBP",
    providerName:"Powerball Providers",
    article_id: 88
  },
  {  itemName:"Ice Cube of Sweden" ,
  category:"Edibles",
  price:"8.6 GBP",
  providerName:"Cold Objects .Inc",
  article_id:102
}]
  private response:any
  private cart:Articles[] = []


  constructor (private http:Http){}


  getItemList(){
     return Promise.resolve(this.itemList)
    }

  getCart(){
   return Promise.resolve(this.cart)
  }

  addToCart(id:number,quantity:number){
    this.cart.push({"article_id":id,"article_quantity":quantity})
}

  removeFromCart(article_id:any){
    this.cart.splice(this.cart.indexOf(article_id),1)
  }

  submitCart(){
    let token = localStorage.getItem('cart_id')
    let toSubmit ={
      "articles":this.cart,
      "language_code":'de',
      "currency_code":'EUR',
      "country_code":'de'
    }
    console.log(token)
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/carts/'+token,JSON.stringify(toSubmit), {headers: headers})
      .map(res => res.json());
  }

  setResponse(resObject:any){
    this.response=resObject
    return this.response
  }
  getResponse(){
    return this.response
  }
}
