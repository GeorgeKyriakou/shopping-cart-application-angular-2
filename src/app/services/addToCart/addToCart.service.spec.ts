import {CartService} from './addToCart.service'
import {Articles} from '../../models/MyCart.model'
import {TestBed, inject} from '@angular/core/testing';

import {FormGroup, ReactiveFormsModule} from '@angular/forms';

describe('>>Service: CartService', ()=>{
  describe('Define service', ()=>{
    it('defined',()=>{
    beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CartService ]
        })
    })
    inject([CartService],(crtService:any) => {
    expect(crtService).toBeDefined()
    })
  })
})

  describe('functions', ()=>{
    var crtService: CartService
    describe('getItemList()-->',()=>{
      it('getItemList()',()=>{
        beforeEach(() => {
          TestBed.configureTestingModule({
            declarations: [CartService],
            imports: [ReactiveFormsModule]
          })
          inject([CartService],(crtService:CartService) => {
          expect(crtService.getItemList.length).toEqual(2)
        })
        })
      })
    })
    describe('addtoCart()-->',()=>{
      it('addtoCart()',()=>{
        beforeEach(() => {
            TestBed.configureTestingModule({
                declarations: [CartService],
                imports: [ReactiveFormsModule]
            })
            inject([CartService],(crtService:CartService) => {
              expect(crtService.getCart.length).toEqual(0)
              crtService.addToCart(88,1)
              expect(crtService.getCart.length).toEqual(1)
          })

          })
        })
    })

    describe('removeFromCart()-->',()=>{
      it('removeFromCart()',()=>{
        beforeEach(() => {
            TestBed.configureTestingModule({
                declarations: [CartService],
                imports: [ReactiveFormsModule]
            })
            inject([CartService],(crtService:CartService) => {
              crtService.addToCart(88,1)
              expect(crtService.getCart.length).toEqual(1)
              crtService.removeFromCart(88)
              expect(crtService.getCart.length).toEqual(0)
            })

          })
        })
    })
})
})
