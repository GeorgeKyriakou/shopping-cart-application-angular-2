"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var CartService = (function () {
    function CartService(http) {
        this.http = http;
        this.itemList = [{
                itemName: "Powerball Display",
                category: "electronics",
                price: "29.99 GBP",
                providerName: "Powerball Providers",
                article_id: 88
            },
            { itemName: "Ice Cube of Sweden",
                category: "Edibles",
                price: "8.6 GBP",
                providerName: "Cold Objects .Inc",
                article_id: 102
            }];
        this.cart = [];
    }
    CartService.prototype.getItemList = function () {
        return Promise.resolve(this.itemList);
    };
    CartService.prototype.getCart = function () {
        return Promise.resolve(this.cart);
    };
    CartService.prototype.addToCart = function (id, quantity) {
        this.cart.push({ "article_id": id, "article_quantity": quantity });
    };
    CartService.prototype.removeFromCart = function (article_id) {
        this.cart.splice(this.cart.indexOf(article_id), 1);
    };
    CartService.prototype.submitCart = function () {
        var token = localStorage.getItem('cart_id');
        var toSubmit = {
            "articles": this.cart,
            "language_code": 'de',
            "currency_code": 'EUR',
            "country_code": 'de'
        };
        console.log(token);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put('https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/carts/' + token, JSON.stringify(toSubmit), { headers: headers })
            .map(function (res) { return res.json(); });
    };
    CartService.prototype.setResponse = function (resObject) {
        this.response = resObject;
        return this.response;
    };
    CartService.prototype.getResponse = function () {
        return this.response;
    };
    return CartService;
}());
CartService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], CartService);
exports.CartService = CartService;
//# sourceMappingURL=addToCart.service.js.map