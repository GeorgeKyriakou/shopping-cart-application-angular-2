"use strict";
var addToCart_service_1 = require("./addToCart.service");
var testing_1 = require("@angular/core/testing");
var forms_1 = require("@angular/forms");
describe('>>Service: CartService', function () {
    describe('Define service', function () {
        it('defined', function () {
            beforeEach(function () {
                testing_1.TestBed.configureTestingModule({
                    providers: [addToCart_service_1.CartService]
                });
            });
            testing_1.inject([addToCart_service_1.CartService], function (crtService) {
                expect(crtService).toBeDefined();
            });
        });
    });
    describe('functions', function () {
        var crtService;
        describe('getItemList()-->', function () {
            it('getItemList()', function () {
                beforeEach(function () {
                    testing_1.TestBed.configureTestingModule({
                        declarations: [addToCart_service_1.CartService],
                        imports: [forms_1.ReactiveFormsModule]
                    });
                    testing_1.inject([addToCart_service_1.CartService], function (crtService) {
                        expect(crtService.getItemList.length).toEqual(2);
                    });
                });
            });
        });
        describe('addtoCart()-->', function () {
            it('addtoCart()', function () {
                beforeEach(function () {
                    testing_1.TestBed.configureTestingModule({
                        declarations: [addToCart_service_1.CartService],
                        imports: [forms_1.ReactiveFormsModule]
                    });
                    testing_1.inject([addToCart_service_1.CartService], function (crtService) {
                        expect(crtService.getCart.length).toEqual(0);
                        crtService.addToCart(88, 1);
                        expect(crtService.getCart.length).toEqual(1);
                    });
                });
            });
        });
        describe('removeFromCart()-->', function () {
            it('removeFromCart()', function () {
                beforeEach(function () {
                    testing_1.TestBed.configureTestingModule({
                        declarations: [addToCart_service_1.CartService],
                        imports: [forms_1.ReactiveFormsModule]
                    });
                    testing_1.inject([addToCart_service_1.CartService], function (crtService) {
                        crtService.addToCart(88, 1);
                        expect(crtService.getCart.length).toEqual(1);
                        crtService.removeFromCart(88);
                        expect(crtService.getCart.length).toEqual(0);
                    });
                });
            });
        });
    });
});
//# sourceMappingURL=addToCart.service.spec.js.map