"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var GetTokenService = (function () {
    function GetTokenService(http) {
        this.http = http;
        this.token = "";
        console.log('log service');
    }
    GetTokenService.prototype.getToken = function (options) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/', JSON.stringify(options), { headers: headers })
            .map(function (res) { return res.json(); });
    };
    GetTokenService.prototype.storeToken = function (token) {
        localStorage.setItem('cart_id', token);
        this.token = token;
    };
    return GetTokenService;
}());
GetTokenService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], GetTokenService);
exports.GetTokenService = GetTokenService;
//# sourceMappingURL=getToken.service.js.map