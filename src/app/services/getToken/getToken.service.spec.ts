import {GetTokenService} from './getToken.service'
import {Articles} from '../../models/MyCart.model'
import {TestBed, inject} from '@angular/core/testing';

import {FormGroup, ReactiveFormsModule} from '@angular/forms';

describe('>>Service: TokenService', ()=>{
  describe('Define service', ()=>{
    it('defined',()=>{
    beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetTokenService ]
        })
    })
    inject([GetTokenService],(tkService:any) => {
    expect(tkService).toBeDefined()
    })
  })
})

describe('Get Token service', ()=>{
  it('getToken()',()=>{
  beforeEach(() => {
  TestBed.configureTestingModule({
    providers: [GetTokenService ]
        })
      })
      inject([GetTokenService],(tkService:GetTokenService) => {
        let options:Object ={"country_code" :"de"}
        expect(tkService.token).toBe("")
        tkService.getToken(options)
        expect(tkService.token.length).toBeGreaterThan(0)
      })
    })
  })
})
