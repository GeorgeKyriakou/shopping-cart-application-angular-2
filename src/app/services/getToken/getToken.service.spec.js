"use strict";
var getToken_service_1 = require("./getToken.service");
var testing_1 = require("@angular/core/testing");
describe('>>Service: TokenService', function () {
    describe('Define service', function () {
        it('defined', function () {
            beforeEach(function () {
                testing_1.TestBed.configureTestingModule({
                    providers: [getToken_service_1.GetTokenService]
                });
            });
            testing_1.inject([getToken_service_1.GetTokenService], function (tkService) {
                expect(tkService).toBeDefined();
            });
        });
    });
    describe('Get Token service', function () {
        it('getToken()', function () {
            beforeEach(function () {
                testing_1.TestBed.configureTestingModule({
                    providers: [getToken_service_1.GetTokenService]
                });
            });
            testing_1.inject([getToken_service_1.GetTokenService], function (tkService) {
                var options = { "country_code": "de" };
                expect(tkService.token).toBe("");
                tkService.getToken(options);
                expect(tkService.token.length).toBeGreaterThan(0);
            });
        });
    });
});
//# sourceMappingURL=getToken.service.spec.js.map