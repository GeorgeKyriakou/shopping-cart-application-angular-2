import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GetTokenService{
  token:string="";

  constructor (private http:Http){
    console.log('log service');
  }


  getToken(options:any){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/',JSON.stringify(options), {headers: headers})
      .map(res => res.json());
  }

  storeToken(token:string){
    localStorage.setItem('cart_id',token);
    this.token = token;
  }
}
