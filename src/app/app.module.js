"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var app_component_1 = require("./app.component");
var landingPage_component_1 = require("./components/landingPage/landingPage.component");
var pageNotFound_component_1 = require("./components/pageNotFound/pageNotFound.component");
var shoppingCart_component_1 = require("./components/shoppingCart/shoppingCart.component");
var purchased_component_1 = require("./components/purchased/purchased.component");
var addToCart_service_1 = require("./services/addToCart/addToCart.service");
var appRoutes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: landingPage_component_1.LandingPageComponent },
    { path: 'my_cart', children: [
            { path: '', component: shoppingCart_component_1.CartComponent },
            { path: 'purchased', component: purchased_component_1.PurchaseComponent }
        ] },
    { path: '**', component: pageNotFound_component_1.PageNotFoundComponent },
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, http_1.HttpModule, forms_1.FormsModule,
            router_1.RouterModule.forRoot(appRoutes, { useHash: true })],
        declarations: [app_component_1.AppComponent, landingPage_component_1.LandingPageComponent,
            pageNotFound_component_1.PageNotFoundComponent, shoppingCart_component_1.CartComponent, purchased_component_1.PurchaseComponent],
        providers: [addToCart_service_1.CartService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map