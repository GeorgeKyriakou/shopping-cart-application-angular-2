import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';



import { AppComponent }  from './app.component';
import {LandingPageComponent} from './components/landingPage/landingPage.component'
import {PageNotFoundComponent} from './components/pageNotFound/pageNotFound.component'
import {CartComponent} from './components/shoppingCart/shoppingCart.component'
import {PurchaseComponent} from './components/purchased/purchased.component'
import {CartService} from './services/addToCart/addToCart.service'


const appRoutes: Routes = [
  {path: '', redirectTo:'home', pathMatch: 'full' },
  {path: 'home', component: LandingPageComponent},
  {path: 'my_cart', children:[
    {path: '', component: CartComponent},
    {path: 'purchased', component: PurchaseComponent}
  ]},

  {path: '**', component: PageNotFoundComponent},
  ];

@NgModule({
  imports:      [ BrowserModule,HttpModule,FormsModule,
                  RouterModule.forRoot(appRoutes, {useHash: true}) ],

  declarations: [ AppComponent, LandingPageComponent,
                  PageNotFoundComponent,CartComponent,PurchaseComponent ],

  providers   : [CartService],

  bootstrap:    [ AppComponent ]
})
export class AppModule { }
