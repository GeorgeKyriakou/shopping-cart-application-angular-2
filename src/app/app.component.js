"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var getToken_service_1 = require("./services/getToken/getToken.service");
var addToCart_service_1 = require("./services/addToCart/addToCart.service");
var AppComponent = (function () {
    function AppComponent(gtService, crtService) {
        this.gtService = gtService;
        this.crtService = crtService;
        this.options = { "country_code": "de" };
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getToken(this.options)
            .then(function (result) {
            if (result) {
                var TOKEN = result['token'];
                _this.gtService.storeToken(TOKEN);
            }
        });
    };
    AppComponent.prototype.getToken = function (options) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.gtService.getToken(_this.options).subscribe(function (res) {
                resolve(res);
            });
        });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'my-app',
        templateUrl: 'app.component.html',
        styleUrls: ['app.component.css'],
        providers: [getToken_service_1.GetTokenService]
    }),
    __metadata("design:paramtypes", [getToken_service_1.GetTokenService,
        addToCart_service_1.CartService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map