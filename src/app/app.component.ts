import { Component,OnInit } from '@angular/core';
import {GetTokenService} from './services/getToken/getToken.service';
import { CartService } from './services/addToCart/addToCart.service'

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  providers: [GetTokenService]
})
export class AppComponent implements OnInit{
   options:Object ={"country_code" :"de"}

  constructor(private gtService:GetTokenService,
              private crtService:CartService
              ){

      }

ngOnInit(){
  this.getToken(this.options)
      .then(result =>{
        if(result){
        const TOKEN = result['token']
        this.gtService.storeToken(TOKEN)
        }
      })
}

  getToken(options:Object){
    return new Promise(resolve =>{
      this.gtService.getToken(this.options).subscribe(res =>{
      resolve(res)
      })
    })
  }
}
