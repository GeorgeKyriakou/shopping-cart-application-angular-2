import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import {CartService} from './app/services/addToCart/addToCart.service'

platformBrowserDynamic().bootstrapModule(AppModule, [CartService]);
